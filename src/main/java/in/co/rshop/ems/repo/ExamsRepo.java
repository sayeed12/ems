package in.co.rshop.ems.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.co.rshop.ems.entities.Exams;

@Repository
public interface ExamsRepo extends JpaRepository<Exams, Long> {

}